const express = require('express');
const router = express.Router();
const fs = require('fs');
const nodemailer = require('nodemailer');

router.post('/create', function(req, res, next) {
  // Save in JSON
  let data = req.body;

  if (data.plz && data.birthday && data.mail) {
    // date to timestamp
    let myDate = data.birthday;
    myDate = myDate.split('-');
    let newDate = new Date( myDate[0], myDate[1] - 1, myDate[2]);
    data.birthday = newDate.getTime();
    let rawdata = fs.readFileSync('checks.json');
    let checks = JSON.parse(rawdata) || [];
    checks.push(data);
    // stringify JSON Object
    let jsonContent = JSON.stringify(checks);
    fs.writeFile('checks.json', jsonContent, 'utf8', function (err) {
      if (err) {
        console.log('An error occured while writing JSON Object to File.');
        res.render('check-created', { title: 'Impftermin-Benachrichtigung', message: 'Benachrichtigung konnte nicht angelegt werden' });
        return console.log(err);
      }

      // Bestätigungsmail verschicken
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'moritz.ellmers@gmail.com',
          pass: process.env.GMAIL_PASSWORD
        }
      });

      // Versende Mail
      let mailOptions = {
        from: 'moritz.ellmers@gmail.com',
        to: data.mail,
        subject: 'Aktivierung der Impftermin-Benachrichtigung',
        html:
            '<h1>Impftermin-Benachrichtigung</h1>' +
            '<p>Du erhälst ab jetzt eine Benachrichtigung, sobald kurzfristig Impfstoff im Impfzentrum deines Postleitzahl-Gebietes (' + data.plz + ') verfügbar ist. <br/> Der Service überprüft alle 60 Sekunden das Impfportal Niedersachsen auf Veränderungen.</p>' +
            '<p>Für weitere Informationen wende dich bitte an das <a href="https://www.impfportal-niedersachsen.de/">Impfportal des Landes Niedersachsen</a>.</p>'
      };
      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });

      res.render('check-created', { title: 'Impftermin-Benachrichtigung', message: 'Benachrichtigung erfolgreich angelegt' });
    });
  }

});

module.exports = router;
