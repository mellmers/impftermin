require('dotenv').config()

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const fs = require('fs');
const nodemailer = require('nodemailer');
const axios = require('axios');

let indexRouter = require('./routes/index');
let checkRouter = require('./routes/check');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/check', checkRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Datei auslesen und Abfragen starten
setInterval(function () {
  let rawdata = fs.readFileSync('checks.json');
  let checks = JSON.parse(rawdata) || [];

  checks.forEach( (check, index) => {
    axios.get('https://www.impfportal-niedersachsen.de/portal/rest/appointments/findVaccinationCenterListFree/' + check.plz + '?stiko=&count=1&birthdate=' + check.birthday).then( res => {
      if (res && res.status && res.data) {
        let succeeded = res.data.succeeded;
        let result = res.data.resultList;

        if (succeeded && result) {
          // Vergleiche lastResponse und verschicke Mail, falls unterschiedlich
          if (checks[index].lastResponse && JSON.stringify(checks[index].lastResponse) != JSON.stringify(result[0])) {
            console.log('----- Versende E-Mail -----')
            let transporter = nodemailer.createTransport({
              service: 'gmail',
              auth: {
                user: 'moritz.ellmers@gmail.com',
                pass: process.env.GMAIL_PASSWORD
              }
            });

            // Versende Mail
            let mailOptions = {
              from: 'moritz.ellmers@gmail.com',
              to: check.mail,
              subject: 'Impftermin-Benachrichtigung',
              html:
                  '<h1>Impftermin-Benachrichtigung</h1>' +
                  '<p>Bitte schaue umgehend auf dem offiziellen <a href="https://www.impfportal-niedersachsen.de/">Impfportal des Landes Niedersachsen</a> nach, ob ein Termin frei ist!</p>' +
                  '<h5>Alte Daten:</h5>' +
                  '<code><pre>' + JSON.stringify(checks[index].lastResponse, undefined, 2) + '</pre></code>' +
                  '<h5>Neue Daten:</h5>' +
                  '<code><pre>' + JSON.stringify(result[0], undefined, 2) + '</pre></code>'
            };
            transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log(error);
              } else {
                console.log('Email sent: ' + info.response);
              }
            });
          }

          checks[index].lastResponse = result[0];
        }

        // Speichere JSON
        let jsonContent = JSON.stringify(checks);
        fs.writeFile('checks.json', jsonContent, 'utf8', function (err) {
          if (err) {
            console.log('An error occured while writing JSON Object to File.');
            return console.log(err);
          }
        });
      }
    });
  });

}, 1000 * 20); // Alle 20 Sekunden wiederholen


module.exports = app;
